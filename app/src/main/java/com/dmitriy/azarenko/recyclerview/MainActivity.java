package com.dmitriy.azarenko.recyclerview;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.NotificationCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button button;

    public ArrayList<Goods> listOfGoods = new ArrayList<>();
    RecyclerViewAdapter adapter;
    Handler h = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == 1){

                int count = listOfGoods.get(0).getCount() - 5;
                listOfGoods.get(0).setCount(count);
                adapter.notifyDataSetChanged();

                h.sendEmptyMessageDelayed(1,1000);



            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listOfGoods.add(new Goods("Milk", 50));
        listOfGoods.add(new Goods("Orange", 50));
        listOfGoods.add(new Goods("Meat", 50));
        listOfGoods.add(new Goods("Chocolate", 50));
        listOfGoods.add(new Goods("Sugar", 50));



        RecyclerView list = (RecyclerView) findViewById(R.id.listView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        list.setLayoutManager(layoutManager);
        adapter = new RecyclerViewAdapter();
        list.setAdapter(adapter);


        h.sendEmptyMessage(1);




    }




    class Goods {


        String goods;
        int count;

        public Goods(String goods, int count) {
            this.goods = goods;
            this.count = count;
        }
        public String getGoods(){
            return goods;
        }
        public void setGoods(String goods){

            this.goods = goods;
        }
        public int getCount(){
            return count;
        }
        public void setCount(int count){

            this.count = count;
        }
    }

    public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>{


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {
            View v = getLayoutInflater().inflate(R.layout.list_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            holder.goods.setText(listOfGoods.get(i).getGoods());
            holder.count.setText(String.valueOf(listOfGoods.get(i).getCount()));


        }

        @Override
        public int getItemCount() {
            return listOfGoods.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private TextView goods;
            private TextView count;


            public ViewHolder(View itemView) {
                super(itemView);
                goods = (TextView) itemView.findViewById(R.id.text_goods_id);
                count = (TextView) itemView.findViewById(R.id.text_Count_id);
            }
        }
    }







}
